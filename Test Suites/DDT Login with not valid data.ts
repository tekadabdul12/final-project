<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>DDT Login with not valid data</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>ef5c5c22-9be9-4c34-a3f5-45fb42953fb0</testSuiteGuid>
   <testCaseLink>
      <guid>4bd9c0a4-2e40-4b3f-9e24-b98c2fbe98fd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DDT Login with not valid data</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>1469dab2-dde4-4bd4-8aef-d80cad06a398</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DDT Login</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>1469dab2-dde4-4bd4-8aef-d80cad06a398</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Email</value>
         <variableId>c29944da-ae5b-4e20-ba37-6409b380ffff</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>1469dab2-dde4-4bd4-8aef-d80cad06a398</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>439b2b67-184c-4bdf-bbf4-1199dade2a7b</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
