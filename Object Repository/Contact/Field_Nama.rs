<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Field_Nama</name>
   <tag></tag>
   <elementGuidId>2f01780d-6dce-4823-9b27-f9e5c0c3ab12</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#nama</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='nama']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>68df3878-f399-48b9-af9a-25cabf24ee46</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>2542f666-7745-4e8d-83e0-4981dfc05874</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>name</value>
      <webElementGuid>1bbaac88-76dc-41d5-ab5c-df93d1b8b093</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>0c9dacd9-888a-4b68-b650-b56a5d1bf778</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>nama</value>
      <webElementGuid>e678874a-21c2-45f1-82c8-b78306afa6fd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>emailHelp</value>
      <webElementGuid>7a9234e1-a075-41a5-9ddd-c4f2fb3eec8e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Enter name</value>
      <webElementGuid>fd57186e-1c71-4d8f-bf3e-500fcb59e603</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;nama&quot;)</value>
      <webElementGuid>ce5af301-18b1-4d27-bc77-ff2708e9271e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='nama']</value>
      <webElementGuid>8155060b-2b25-453d-8998-96c8c747d433</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/input</value>
      <webElementGuid>b98b5730-5c40-4d7d-9b66-5669e9d01ee8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text' and @name = 'name' and @id = 'nama' and @placeholder = 'Enter name']</value>
      <webElementGuid>c1ee992c-c1a2-4b52-8773-d0e6469529c4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
