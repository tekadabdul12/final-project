<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Field_E-Mail</name>
   <tag></tag>
   <elementGuidId>ab74ed12-bef0-450f-bb88-ee5c6c384129</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#email</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='email']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>0d144c4b-233f-4ef1-b83b-6d76685a18db</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>email</value>
      <webElementGuid>e662d6eb-c390-46d8-9ea6-41b66af4a65e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>contoh@mail.com</value>
      <webElementGuid>1f2f5529-17e1-46c5-88a2-0c1326310955</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>email</value>
      <webElementGuid>69c135b4-a01c-4d29-a152-fadc6e6970ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control </value>
      <webElementGuid>a21532d5-bce1-41b5-b36d-c0552fefd2e2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>email</value>
      <webElementGuid>6a4b7815-23ee-4003-8d47-56b6d007f522</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>email</value>
      <webElementGuid>11390d90-6ae9-4777-b497-7dbbc491ada4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;email&quot;)</value>
      <webElementGuid>685e9f35-3970-45b7-9eb0-fb76f2154e35</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='email']</value>
      <webElementGuid>599e0a25-3e15-4384-8c96-b19a626fa5ed</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/input</value>
      <webElementGuid>08f6e460-7580-41cc-a83b-41c195c6cd84</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'email' and @placeholder = 'contoh@mail.com' and @type = 'email' and @name = 'email']</value>
      <webElementGuid>4c726a86-3435-4803-a1f2-cfde742c66e7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
