<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Btn_Sign-in With Google</name>
   <tag></tag>
   <elementGuidId>508d2c75-c5b5-4bee-9dc5-49c1404372f8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#buttonGoogleTrack</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='buttonGoogleTrack']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>72ef5a89-889d-4deb-8c0e-375d1a2a1cb2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>buttonGoogleTrack</value>
      <webElementGuid>b488d519-c727-4a15-88fe-8acfd3657308</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>7f271d52-efe0-4eb1-a052-5d85ae273e3d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn wm-all-events btn-block bg-white</value>
      <webElementGuid>8b602a24-46db-46e2-a9fe-a53b845e28f2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                            
                                            Sign-in With Google
                                        </value>
      <webElementGuid>48fdc200-c2fa-42a7-b874-b5db3b10bcf3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;buttonGoogleTrack&quot;)</value>
      <webElementGuid>fde44373-c6f9-4bbc-b87f-a4aa3a5ba785</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='buttonGoogleTrack']</value>
      <webElementGuid>e71bf1b5-f2d4-4938-af7e-01f2156e3a6d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Daftar'])[1]/following::button[1]</value>
      <webElementGuid>2e09574d-f8ab-4769-ade1-d5757d22151a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='syarat dan ketentuan'])[1]/following::button[2]</value>
      <webElementGuid>c379fba7-017f-4b8b-8668-76e9988ec031</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Sign-in With Google']/parent::*</value>
      <webElementGuid>8c00349c-9fb4-4c25-823f-f24826f141df</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/button</value>
      <webElementGuid>bb0eae94-dbdc-4ef6-9784-4ee8c122221d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'buttonGoogleTrack' and @type = 'button' and (text() = '
                                            
                                            Sign-in With Google
                                        ' or . = '
                                            
                                            Sign-in With Google
                                        ')]</value>
      <webElementGuid>c24d9600-bc5d-4a5b-a4c9-533f390ec126</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
