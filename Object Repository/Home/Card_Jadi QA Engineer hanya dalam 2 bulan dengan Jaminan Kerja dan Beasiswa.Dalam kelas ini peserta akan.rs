<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Card_Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa.Dalam kelas ini peserta akan</name>
   <tag></tag>
   <elementGuidId>5948bfc9-e9ea-4db0-9341-1ca9b39fa442</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.containerCardBootcamp > p.new_body2_regular</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-body-homepage']/div[2]/div/ul/li/div/div/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>e138e929-36c9-4d39-8a52-dc11e9953628</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>new_body2_regular</value>
      <webElementGuid>2c881986-abf3-4008-b613-ba7ea3003141</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                    Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.Dalam kelas ini peserta akan...</value>
      <webElementGuid>b446ad97-71d2-482b-9816-ffa58817f2c3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-body-homepage&quot;)/div[@class=&quot;bootcampBlock&quot;]/div[@class=&quot;blockBootcamp&quot;]/ul[@class=&quot;listBootcamp&quot;]/li[@class=&quot;listBootcampItem&quot;]/div[@class=&quot;cardBootcamp&quot;]/div[@class=&quot;containerCardBootcamp&quot;]/p[@class=&quot;new_body2_regular&quot;]</value>
      <webElementGuid>351af984-5241-464c-8e1a-93b5cf97a112</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-body-homepage']/div[2]/div/ul/li/div/div/p</value>
      <webElementGuid>fbb511eb-d0ac-49e4-9fe1-3f9b3e96c29c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Advance Level'])[3]/following::p[1]</value>
      <webElementGuid>43c5abff-e388-40c0-a6f2-9eb346aa75eb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quality Assurance Engineer Class'])[3]/following::p[1]</value>
      <webElementGuid>b77033e7-bef2-4489-84df-8f680105c590</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quality Assurance Engineer Class'])[4]/preceding::p[1]</value>
      <webElementGuid>151ec8c4-a221-4fe0-b3f4-ca2620635dab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Materi Belajar'])[1]/preceding::p[1]</value>
      <webElementGuid>792a002c-9e2e-4bae-8f49-d70d1a46a338</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.Dalam kelas ini peserta akan...']/parent::*</value>
      <webElementGuid>9126ba9f-25b7-4bcb-ab69-06af9ce68542</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/div/div/p</value>
      <webElementGuid>fad0489a-69dd-4a46-b715-5504897059bc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = '
                                    Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.Dalam kelas ini peserta akan...' or . = '
                                    Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.Dalam kelas ini peserta akan...')]</value>
      <webElementGuid>2e50cd6f-eb8d-42ff-bc9a-4130164f9244</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
