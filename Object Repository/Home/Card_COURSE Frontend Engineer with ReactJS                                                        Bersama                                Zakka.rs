<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Card_COURSE Frontend Engineer with ReactJS                                                        Bersama                                Zakka</name>
   <tag></tag>
   <elementGuidId>4070d5e1-b2c3-464d-9d4e-b64fb34a9dad</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@onclick=&quot;window.location.href='https://demo-app.online/course/frontend-engineer-with-reactjs-atgh0cxd2e5n';&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>9c79644f-98a9-4a37-b4a0-6c74822e3094</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-course</name>
      <type>Main</type>
      <value>Frontend Engineer with ReactJS</value>
      <webElementGuid>6bd8fc37-e942-4ab4-9200-020595068246</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>window.location.href='https://demo-app.online/course/frontend-engineer-with-reactjs-atgh0cxd2e5n';</value>
      <webElementGuid>50c815ce-1f72-4272-9419-b091329809c3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>cardBootcamp list-online-course-item-container </value>
      <webElementGuid>3d49dba5-b464-4621-ac05-daa188b07389</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        
                        
                            COURSE
                            
                                Frontend Engineer with ReactJS
                            
                            Bersama
                                Zakka
                                dari
                                
                            
                                73 Video | 8h 31m
                            
                                                                                                Rp
                                        5.000.000
                                    
                                                            
                        

                    </value>
      <webElementGuid>ab06e839-65ef-4443-8635-4090b717ab47</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-body-homepage&quot;)/div[@class=&quot;online-course-block&quot;]/ul[@class=&quot;list-online-course-container&quot;]/li[@class=&quot;list-online-course-item&quot;]/div[@class=&quot;cardBootcamp list-online-course-item-container&quot;]</value>
      <webElementGuid>d4757656-27d1-4b2b-8606-949e4aae3732</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@onclick=&quot;window.location.href='https://demo-app.online/course/frontend-engineer-with-reactjs-atgh0cxd2e5n';&quot;]</value>
      <webElementGuid>6365ff71-5f68-4497-b76a-c6fd8ab8beca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-body-homepage']/div[5]/ul/li[2]/div</value>
      <webElementGuid>cf2d6c28-fe93-4330-bd8d-3230ced9e25b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Basic Programming with Javascript'])[1]/following::div[1]</value>
      <webElementGuid>c2eec1b3-9275-4fb5-8178-cf7cca6b8936</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='COURSE'])[1]/following::div[1]</value>
      <webElementGuid>f607a6f6-63ad-4582-90e7-e5b2c4fae1fe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/ul/li[2]/div</value>
      <webElementGuid>92f7ff3b-c2bb-475f-9bde-31d67dec5633</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        
                        
                            COURSE
                            
                                Frontend Engineer with ReactJS
                            
                            Bersama
                                Zakka
                                dari
                                
                            
                                73 Video | 8h 31m
                            
                                                                                                Rp
                                        5.000.000
                                    
                                                            
                        

                    ' or . = '
                        
                        
                            COURSE
                            
                                Frontend Engineer with ReactJS
                            
                            Bersama
                                Zakka
                                dari
                                
                            
                                73 Video | 8h 31m
                            
                                                                                                Rp
                                        5.000.000
                                    
                                                            
                        

                    ')]</value>
      <webElementGuid>0fe002f8-c9ec-4cc4-92a0-9a56083c007b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
