<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Card_COURSE Basic Programming with Javascript                                                        Bersama                                Zakka</name>
   <tag></tag>
   <elementGuidId>b137f047-69fe-4992-9e4e-210d5ef7eb8a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@onclick=&quot;window.location.href='https://demo-app.online/course/basic-programming-with-javascript-eci19jm45dtb';&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>f6423bfe-2e09-42ec-883b-7682c7455983</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-course</name>
      <type>Main</type>
      <value>Basic Programming with Javascript</value>
      <webElementGuid>d6d8296e-c1ef-4d56-94f8-0505acc6f88f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>window.location.href='https://demo-app.online/course/basic-programming-with-javascript-eci19jm45dtb';</value>
      <webElementGuid>cbfd0bdc-0f88-4bc6-847a-44cae8500f9d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>cardBootcamp list-online-course-item-container </value>
      <webElementGuid>ce466385-7cbc-4d06-8097-0aa0478f3eaa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        
                        
                            COURSE
                            
                                Basic Programming with Javascript
                            
                            Bersama
                                Zakka
                                dari
                                
                            
                                27 Video | 7h 53m
                            
                                                                                                Rp
                                        2.500.000
                                    
                                                            
                        

                    </value>
      <webElementGuid>a22af27e-dcee-442f-8509-4eb9297f9b6d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-body-homepage&quot;)/div[@class=&quot;online-course-block&quot;]/ul[@class=&quot;list-online-course-container&quot;]/li[@class=&quot;list-online-course-item&quot;]/div[@class=&quot;cardBootcamp list-online-course-item-container&quot;]</value>
      <webElementGuid>6c0745fd-58e9-4ec7-b47a-af828acfa2d1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@onclick=&quot;window.location.href='https://demo-app.online/course/basic-programming-with-javascript-eci19jm45dtb';&quot;]</value>
      <webElementGuid>dae91b08-06cc-4788-9ae2-28f7be127b2c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-body-homepage']/div[5]/ul/li/div</value>
      <webElementGuid>af52add1-cf83-4d52-8ebc-eac5e7cc53f8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Online Course'])[1]/following::div[1]</value>
      <webElementGuid>0dc359b6-ea11-48c3-b58b-3101f37fce94</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/ul/li/div</value>
      <webElementGuid>53ffb2d0-4fa4-4ee8-97a2-9099b3e65567</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        
                        
                            COURSE
                            
                                Basic Programming with Javascript
                            
                            Bersama
                                Zakka
                                dari
                                
                            
                                27 Video | 7h 53m
                            
                                                                                                Rp
                                        2.500.000
                                    
                                                            
                        

                    ' or . = '
                        
                        
                            COURSE
                            
                                Basic Programming with Javascript
                            
                            Bersama
                                Zakka
                                dari
                                
                            
                                27 Video | 7h 53m
                            
                                                                                                Rp
                                        2.500.000
                                    
                                                            
                        

                    ')]</value>
      <webElementGuid>8077e459-f19a-4f27-9488-05d93971f7cb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
