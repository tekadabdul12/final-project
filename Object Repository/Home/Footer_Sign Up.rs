<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Footer_Sign Up</name>
   <tag></tag>
   <elementGuidId>d6702e9d-8065-4843-8153-bfa2c6117b4e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//footer[@id='wm-footer']/div/div/div/aside[2]/div[2]/ul[2]/li[3]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>7945908f-f53e-4546-988c-6e4007f962de</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>listSiteMap</value>
      <webElementGuid>edeab907-2b8a-406b-8d17-ed18ef9c44df</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/daftar?</value>
      <webElementGuid>3f420e4e-2e07-4b05-b687-4ecce0365b8b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                Sign Up</value>
      <webElementGuid>d3fc111c-7211-43fe-af33-0e2d66b09b54</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;wm-footer&quot;)/div[1]/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/aside[@class=&quot;col-md-4 col-sm-12 col-12&quot;]/div[@class=&quot;row&quot;]/ul[@class=&quot;col-md-6 col-sm-6 col-xs-6&quot;]/li[3]/a[@class=&quot;listSiteMap&quot;]</value>
      <webElementGuid>b57e9a15-9401-4590-88aa-962d2a5585bd</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//footer[@id='wm-footer']/div/div/div/aside[2]/div[2]/ul[2]/li[3]/a</value>
      <webElementGuid>9ef5ea8b-c17c-4dba-a549-53e06a6b2efd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Sign Up')]</value>
      <webElementGuid>8c7aeff3-24cb-47e0-a3b3-b8543086d97b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Login'])[2]/following::a[1]</value>
      <webElementGuid>9e8b23f0-c039-4018-8534-c2535810af66</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kontak'])[4]/following::a[2]</value>
      <webElementGuid>6cb5c68b-12c5-488b-b6e6-a34bbd4bbab3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kontak'])[5]/preceding::a[1]</value>
      <webElementGuid>b914e499-abfe-401d-9b39-7d4b148ec559</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Course'])[6]/preceding::a[6]</value>
      <webElementGuid>5a019820-c166-45d6-902b-717e173a9676</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Sign Up']/parent::*</value>
      <webElementGuid>ab45f1be-a909-48de-a8bf-d5605c0e63cf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/daftar?')]</value>
      <webElementGuid>9da1040d-75e8-41c4-9e83-797248fd1b0d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/ul[2]/li[3]/a</value>
      <webElementGuid>8d7fffe3-4d5f-494c-9b70-7534adf7cc32</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/daftar?' and (text() = '
                                                Sign Up' or . = '
                                                Sign Up')]</value>
      <webElementGuid>a1fc6cb1-9c57-4faf-819c-22573850e651</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
