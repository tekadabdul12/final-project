<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Card_EVENT Day 4 Workshop bersama Ziyad Syauqi Fawwazi, Data Scientist Mark</name>
   <tag></tag>
   <elementGuidId>7893f701-bfdb-466c-b9c3-ba7cbcaec0af</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-body-homepage']/div[6]/ul/li[2]/div/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>2ebd5f28-c7c1-4748-8e0f-99b33260bc5d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>containerCardBootcamp</value>
      <webElementGuid>0870c6e5-1a82-4ba0-8182-fc8221a75440</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            EVENT
                            
                                Day 4: Workshop
                            
                            
                                bersama Ziyad Syauqi Fawwazi, Data Scientist Market Leader Company in Automotive Industry
                            
                            
                                25 Nov 2023,
                                19:30
                                WIB | Via
                                Zoom
                            

                                                            
                                    Open Registration until 25 Nov 2023 11:30 WIB
                                
                            
                                                                                                                                        
                                            Rp. 500.000
                                        Rp.
                                            85.000
                                        
                                                                                                

                        </value>
      <webElementGuid>4bee3ca8-b63a-461e-a248-a2ebe35b26f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-body-homepage&quot;)/div[@class=&quot;online-course-block&quot;]/ul[@class=&quot;list-online-course-container&quot;]/li[@class=&quot;list-online-course-item&quot;]/div[@class=&quot;cardBootcamp list-online-course-item-container&quot;]/div[@class=&quot;containerCardBootcamp&quot;]</value>
      <webElementGuid>44e034f1-b4b4-4722-a881-8a286acc1b65</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-body-homepage']/div[6]/ul/li[2]/div/div[2]</value>
      <webElementGuid>6e552450-5207-4c96-9c11-30cecc8a6c47</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Data Scientist Market Leader Company in Automotive Industry'])[2]/following::div[1]</value>
      <webElementGuid>e7922c14-b197-4f64-bd75-ba814b8394ef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ziyad Syauqi Fawwazi'])[2]/following::div[1]</value>
      <webElementGuid>98263a8d-74a2-4f42-8686-76e99508dd74</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[2]/div/div[2]</value>
      <webElementGuid>5927cf90-618b-4946-b151-4f71331df25b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                            EVENT
                            
                                Day 4: Workshop
                            
                            
                                bersama Ziyad Syauqi Fawwazi, Data Scientist Market Leader Company in Automotive Industry
                            
                            
                                25 Nov 2023,
                                19:30
                                WIB | Via
                                Zoom
                            

                                                            
                                    Open Registration until 25 Nov 2023 11:30 WIB
                                
                            
                                                                                                                                        
                                            Rp. 500.000
                                        Rp.
                                            85.000
                                        
                                                                                                

                        ' or . = '
                            EVENT
                            
                                Day 4: Workshop
                            
                            
                                bersama Ziyad Syauqi Fawwazi, Data Scientist Market Leader Company in Automotive Industry
                            
                            
                                25 Nov 2023,
                                19:30
                                WIB | Via
                                Zoom
                            

                                                            
                                    Open Registration until 25 Nov 2023 11:30 WIB
                                
                            
                                                                                                                                        
                                            Rp. 500.000
                                        Rp.
                                            85.000
                                        
                                                                                                

                        ')]</value>
      <webElementGuid>bbe8010c-75a4-4b12-979c-6785f46aa969</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
