<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Card_EVENT Day 3 Predict using Machine Learning bersama Ziyad Syauqi Fawwaz</name>
   <tag></tag>
   <elementGuidId>aaa521b4-2304-452b-a6a1-7c89ae728232</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-body-homepage']/div[6]/ul/li/div/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>5d5a7eba-c5f5-49a4-944f-88058d8acabc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>containerCardBootcamp</value>
      <webElementGuid>4dbb5d53-a866-4fd9-8619-0ab37ee54623</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            EVENT
                            
                                Day 3: Predict using Machine Learning
                            
                            
                                bersama Ziyad Syauqi Fawwazi, Data Scientist Market Leader Company in Automotive Industry
                            
                            
                                18 Nov 2023,
                                19:30
                                WIB | Via
                                Zoom
                            

                                                            
                                    Open Registration until 18 Nov 2023 11:30 WIB
                                
                            
                                                                                                                                        
                                            Rp. 500.000
                                        Rp.
                                            85.000
                                        
                                                                                                

                        </value>
      <webElementGuid>1ee5e9b9-37d6-465e-a615-97cd5c43a580</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-body-homepage&quot;)/div[@class=&quot;online-course-block&quot;]/ul[@class=&quot;list-online-course-container&quot;]/li[@class=&quot;list-online-course-item&quot;]/div[@class=&quot;cardBootcamp list-online-course-item-container&quot;]/div[@class=&quot;containerCardBootcamp&quot;]</value>
      <webElementGuid>e1c16467-1398-4203-8117-f7a730d0f624</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-body-homepage']/div[6]/ul/li/div/div[2]</value>
      <webElementGuid>45401a62-922d-4a05-b81d-a66692262f22</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Data Scientist Market Leader Company in Automotive Industry'])[1]/following::div[1]</value>
      <webElementGuid>208d0cdd-d16f-428d-bb47-01bd73ab6dfe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ziyad Syauqi Fawwazi'])[1]/following::div[1]</value>
      <webElementGuid>e89ad875-fb3a-4dfb-affa-5f5b09f62230</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/div/div[2]</value>
      <webElementGuid>c103094d-5a02-45db-a441-d13805967fda</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                            EVENT
                            
                                Day 3: Predict using Machine Learning
                            
                            
                                bersama Ziyad Syauqi Fawwazi, Data Scientist Market Leader Company in Automotive Industry
                            
                            
                                18 Nov 2023,
                                19:30
                                WIB | Via
                                Zoom
                            

                                                            
                                    Open Registration until 18 Nov 2023 11:30 WIB
                                
                            
                                                                                                                                        
                                            Rp. 500.000
                                        Rp.
                                            85.000
                                        
                                                                                                

                        ' or . = '
                            EVENT
                            
                                Day 3: Predict using Machine Learning
                            
                            
                                bersama Ziyad Syauqi Fawwazi, Data Scientist Market Leader Company in Automotive Industry
                            
                            
                                18 Nov 2023,
                                19:30
                                WIB | Via
                                Zoom
                            

                                                            
                                    Open Registration until 18 Nov 2023 11:30 WIB
                                
                            
                                                                                                                                        
                                            Rp. 500.000
                                        Rp.
                                            85.000
                                        
                                                                                                

                        ')]</value>
      <webElementGuid>c6e1baf5-1806-4f3f-b47d-d84ea330b6d4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
