<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Menu_My Events</name>
   <tag></tag>
   <elementGuidId>3332115a-65c0-4711-a325-eff43a950ba3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//aside[@id='sidebar-wrapper']/ul/li[7]/a/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>b668001d-632b-4f57-9dff-648d38f5a1bc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>My
                                        Events</value>
      <webElementGuid>b6acf34e-c733-4871-be43-d6f4f9a21716</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;sidebar-wrapper&quot;)/ul[@class=&quot;sidebar-menu&quot;]/li[@class=&quot;dropdown&quot;]/a[@class=&quot;nav-link&quot;]/span[1]</value>
      <webElementGuid>c6ad1f31-dcf7-4dee-8ac7-c018987ac13b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//aside[@id='sidebar-wrapper']/ul/li[7]/a/span</value>
      <webElementGuid>e99737e3-e10d-4afd-afb0-d654e8c36462</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Transaction'])[1]/following::span[1]</value>
      <webElementGuid>198f24de-7ba6-42c2-be25-b3865f60dedd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Billing Account'])[1]/following::span[1]</value>
      <webElementGuid>a171dac8-8246-4193-9a95-efcca52cd379</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[7]/a/span</value>
      <webElementGuid>64c2f7a7-d191-4347-89fc-714f07e0f4fe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'My
                                        Events' or . = 'My
                                        Events')]</value>
      <webElementGuid>16a396d1-1e2e-4a75-b15b-aaf800187356</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
