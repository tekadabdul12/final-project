<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Card_My Events</name>
   <tag></tag>
   <elementGuidId>9c237885-a838-42bc-85ce-ab89553687b8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div[3]/section/div/div/div[2]/a/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>9db99f89-23bf-4646-9937-e77d7af8e6ec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card-statistic-4</value>
      <webElementGuid>51b1e108-706e-4603-af9c-2776af340bec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    
                    
                        
                        
                            My Events
                            0
                        
                        
                        
                        
                            
                        
                        
                    
                    
                </value>
      <webElementGuid>5bbbb9bb-1c41-4394-8434-ced805fd2442</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;main-wrapper main-wrapper-1&quot;]/div[@class=&quot;main-content&quot;]/section[@class=&quot;section&quot;]/div[@class=&quot;section-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12&quot;]/a[1]/div[@class=&quot;card&quot;]/div[@class=&quot;card-statistic-4&quot;]</value>
      <webElementGuid>07d06a39-f8d8-4be3-a6df-89834fb0570d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[3]/section/div/div/div[2]/a/div/div</value>
      <webElementGuid>d10a5b56-ca81-43df-b8a6-2bf30d2270c1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My points'])[1]/following::div[5]</value>
      <webElementGuid>829b65ee-e56f-4cb5-ac31-48103ad86802</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Invoice'])[1]/following::div[15]</value>
      <webElementGuid>124bca87-3010-478c-9823-34e547d1840b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Membership'])[1]/preceding::div[7]</value>
      <webElementGuid>66685fbf-431c-4f7e-b6b1-439de50fb3f3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/a/div/div</value>
      <webElementGuid>598d4309-5b07-4117-9f5a-111064f12417</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                    
                    
                        
                        
                            My Events
                            0
                        
                        
                        
                        
                            
                        
                        
                    
                    
                ' or . = '
                    
                    
                        
                        
                            My Events
                            0
                        
                        
                        
                        
                            
                        
                        
                    
                    
                ')]</value>
      <webElementGuid>80cd5a49-dfc6-4d2e-a5ac-63d726d7696d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
