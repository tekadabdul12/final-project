<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Card_Membership</name>
   <tag></tag>
   <elementGuidId>af51e7d3-5d1f-4fa8-aa2b-0a0d0584369c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div[3]/section/div/div/div[3]/a/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>89ae1a67-0357-457e-b252-e492b8001fa8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card-statistic-4</value>
      <webElementGuid>a3d76e02-0ff5-41ee-94de-63ee63c8f71b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    
                    
                        
                        
                            Membership
                            Basic
                        
                        
                        
                        
                            
                        
                        
                    
                    
                </value>
      <webElementGuid>48e85063-6d1a-4b07-a81b-b963c7e3260c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;main-wrapper main-wrapper-1&quot;]/div[@class=&quot;main-content&quot;]/section[@class=&quot;section&quot;]/div[@class=&quot;section-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12&quot;]/a[1]/div[@class=&quot;card&quot;]/div[@class=&quot;card-statistic-4&quot;]</value>
      <webElementGuid>1db443fe-dd94-400d-86f2-b0c8e0023ff4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[3]/section/div/div/div[3]/a/div/div</value>
      <webElementGuid>0fde5139-8723-419d-92a8-b73737a4bc45</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My Events'])[2]/following::div[5]</value>
      <webElementGuid>a6804257-522a-4e0a-a958-825d74387e19</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My points'])[1]/following::div[14]</value>
      <webElementGuid>bfbeb7b4-d9c7-4171-9233-bf7406df31d0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/a/div/div</value>
      <webElementGuid>888e637c-ed63-4b4d-9911-aa72b8e5332b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                    
                    
                        
                        
                            Membership
                            Basic
                        
                        
                        
                        
                            
                        
                        
                    
                    
                ' or . = '
                    
                    
                        
                        
                            Membership
                            Basic
                        
                        
                        
                        
                            
                        
                        
                    
                    
                ')]</value>
      <webElementGuid>e64fe719-84bc-477f-abfb-192ca9c39b4d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
