<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Card_My points</name>
   <tag></tag>
   <elementGuidId>5f3a69da-cd44-4c52-9a66-1adf444da455</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-lg-6.col-md-6.col-sm-6.col-xs-6.pr-0.pt-3</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div[3]/section/div/div/div/a/div/div/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>3c55a6be-65ef-46c2-a223-04120b042c9b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-lg-6 col-md-6 col-sm-6 col-xs-6 pr-0 pt-3</value>
      <webElementGuid>d45c3581-7b85-438c-9344-a953205d1d3f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        
                            My points
                            0
                        
                        </value>
      <webElementGuid>347b0fd2-3481-499d-951f-3ec8ec826d99</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;main-wrapper main-wrapper-1&quot;]/div[@class=&quot;main-content&quot;]/section[@class=&quot;section&quot;]/div[@class=&quot;section-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12&quot;]/a[1]/div[@class=&quot;card&quot;]/div[@class=&quot;card-statistic-4&quot;]/div[@class=&quot;align-items-center justify-content-between&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-lg-6 col-md-6 col-sm-6 col-xs-6 pr-0 pt-3&quot;]</value>
      <webElementGuid>de41b519-0ca8-4e57-a09c-7c598771404e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[3]/section/div/div/div/a/div/div/div/div/div</value>
      <webElementGuid>670e606d-7282-42e4-8af7-44229db16827</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Invoice'])[1]/following::div[9]</value>
      <webElementGuid>bc901c84-5ea2-40c8-9f55-df26a0d9df17</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My Events'])[2]/preceding::div[4]</value>
      <webElementGuid>66fd0935-1031-49d3-99c8-6a7bfb59f311</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/div/div/div/div/div</value>
      <webElementGuid>b11727b0-e4cd-4cd1-b0e4-5e349b0df6d0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        
                            My points
                            0
                        
                        ' or . = '
                        
                            My points
                            0
                        
                        ')]</value>
      <webElementGuid>e4480be8-fa1b-493b-a228-ff0903ece34f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
