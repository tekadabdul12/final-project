<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Btn_Take this course</name>
   <tag></tag>
   <elementGuidId>47742ab6-401a-4927-b781-2e758c51921b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-light.buttonLoad</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='jumbotronCourseDetail']/div/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>14b7cd60-6879-45b2-a217-d42c11ad2a51</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>wire:click</name>
      <type>Main</type>
      <value>addToCart</value>
      <webElementGuid>2ca4a2ed-3ffa-48bc-a65f-8accee400885</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-light buttonLoad</value>
      <webElementGuid>634a209f-6292-4908-b2a7-8abace47c3af</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>x-bind:class</name>
      <type>Main</type>
      <value>modalOpen ? 'modal-open' : ''</value>
      <webElementGuid>9296b8ae-e446-475d-87e5-5a08d71f50e5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Take
                this course
            </value>
      <webElementGuid>12aa2529-4d60-4aad-80f0-212d90d57b43</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;jumbotronCourseDetail&quot;)/div[@class=&quot;textJumbotron&quot;]/button[@class=&quot;btn btn-light buttonLoad&quot;]</value>
      <webElementGuid>394c567d-3d57-45c3-875d-f8fc9eb6390e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='jumbotronCourseDetail']/div/button</value>
      <webElementGuid>658abb87-430d-46a6-906c-6a0faaac23b5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Basic Programming with Javascript'])[1]/following::button[1]</value>
      <webElementGuid>d488dd11-8309-4eca-8d07-d488c5c311ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Back to Course >'])[1]/preceding::button[1]</value>
      <webElementGuid>ca9b7d38-db32-4bc0-ba1e-635a2e57eef9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Basic Programming with Javascript'])[2]/preceding::button[1]</value>
      <webElementGuid>c414dee7-fc99-4dce-8122-440b442817fc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div/button</value>
      <webElementGuid>4f616dbe-464b-492d-a94e-f0cb1654eaa7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Take
                this course
            ' or . = 'Take
                this course
            ')]</value>
      <webElementGuid>d24f27a6-9061-49fa-ad35-4e5e74ed8a47</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
