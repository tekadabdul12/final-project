<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Btn_Beli Tiket</name>
   <tag></tag>
   <elementGuidId>2c2560f1-6c38-4e12-b34d-99aefcb8b4f5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-primary.btn-lg</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='list-button']/form/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>51d5e38d-72f3-4762-8f45-feccd47e79ef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary btn-lg</value>
      <webElementGuid>2cbe95e6-6c4a-4f3a-9dca-e3627d7ee41e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Beli Tiket</value>
      <webElementGuid>109e3b2f-595f-4d0c-9d35-619f7f325561</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;list-button&quot;)/form[1]/button[@class=&quot;btn btn-primary btn-lg&quot;]</value>
      <webElementGuid>4f218652-7037-477c-9f8a-30bdbcadfe5a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='list-button']/form/button</value>
      <webElementGuid>356ca64c-39b1-4210-b465-9a1d48fcea33</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Detik'])[1]/following::button[1]</value>
      <webElementGuid>2bd19871-9bcf-4343-8589-f1744cba0c66</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Menit'])[1]/following::button[1]</value>
      <webElementGuid>1ce3e8e4-80f0-476f-b651-64f9e5938583</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Event lain yang banyak diminati'])[1]/preceding::button[1]</value>
      <webElementGuid>d8402772-711c-480c-8872-9116712b5842</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Day 4: Workshop'])[1]/preceding::button[1]</value>
      <webElementGuid>601a3afe-311b-475c-966d-514d3818010a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Beli Tiket']/parent::*</value>
      <webElementGuid>dd37a29f-8f40-425d-8ace-7a55cac61818</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/ul/li[6]/form/button</value>
      <webElementGuid>305bcca9-ee57-4056-a05d-a06fd86914ab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Beli Tiket' or . = 'Beli Tiket')]</value>
      <webElementGuid>1c2ce899-b7d3-4a67-a560-0b1424475d0f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
