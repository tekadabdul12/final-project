<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Field_Number Holder</name>
   <tag></tag>
   <elementGuidId>baa5f80c-4e92-4f5f-82e9-12815738e128</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@name='nomor_rekening']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>input[name=&quot;nomor_rekening&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>3c759a59-fb7e-44fe-8de0-34c689f92247</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>6a9e17c5-4e93-4259-b75f-9b731b746dd0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>nomor_rekening</value>
      <webElementGuid>3bcaee75-f811-48cc-ae53-d96b898ddcdb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>a8b2c772-5ac8-49c0-9400-b57bb46707f2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>085814636461</value>
      <webElementGuid>0531c3b3-cf7c-4dbf-97ea-ebdbc6e7c1e2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;main-wrapper main-wrapper-1&quot;]/div[@class=&quot;main-content&quot;]/section[@class=&quot;section&quot;]/div[@class=&quot;section-body&quot;]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;col-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;card author-box&quot;]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;col-12 col-md-12 col-lg-6&quot;]/div[@class=&quot;card-body&quot;]/div[@class=&quot;py-4&quot;]/form[1]/p[@class=&quot;clearfix&quot;]/span[@class=&quot;float-right text-muted lead mt-2&quot;]/input[@class=&quot;form-control&quot;]</value>
      <webElementGuid>f95f07ac-6bf6-4314-a8ec-c38541fd73cf</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@name='nomor_rekening']</value>
      <webElementGuid>2ae2ac49-f41c-4e22-9541-55e86516628a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[3]/section/div/div/div/div/div/div/div/div/form/p[3]/span[2]/input</value>
      <webElementGuid>e3858192-eb8f-40d9-8482-1121e2269ff7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//p[3]/span[2]/input</value>
      <webElementGuid>da710604-907c-48d5-ba08-633483673d76</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text' and @name = 'nomor_rekening']</value>
      <webElementGuid>80f4d929-f3ce-4fdd-8f66-10cb5e69066b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
