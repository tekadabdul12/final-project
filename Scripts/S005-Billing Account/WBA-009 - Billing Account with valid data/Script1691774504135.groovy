import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('WLog-001'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.mouseOver(findTestObject('Home/Icon_User'))

WebUI.delay(3)

WebUI.click(findTestObject('Home/Menu_My Account'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Dashboard/Menu_Billing Account'))

WebUI.selectOptionByValue(findTestObject('Billing Account/Dropdown_Wallet'), findTestData('Billing Account').getValue(1, 
        1), false)

WebUI.setText(findTestObject('Billing Account/Field_Name Holder'), findTestData('Billing Account').getValue(2, 1))

WebUI.setText(findTestObject('Billing Account/Field_Number Holder'), findTestData('Billing Account').getValue(3, 1))

WebUI.takeFullPageScreenshot()

WebUI.click(findTestObject('Billing Account/Btn_Submit'))

WebUI.takeFullPageScreenshot()

WebUI.closeBrowser()

