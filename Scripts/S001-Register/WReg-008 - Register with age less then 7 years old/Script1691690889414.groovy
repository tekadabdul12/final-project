import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demo-app.online/')

WebUI.click(findTestObject('Home/Btn_Buat Akun'))

WebUI.setText(findTestObject('Register/Field_Nama'), findTestData('Register').getValue(1, 1))

WebUI.setText(findTestObject('Register/Field_Tanggal lahir'), findTestData('Register').getValue(2, 2))

WebUI.setText(findTestObject('Register/Field_E-Mail'), findTestData('Register').getValue(3, 4))

WebUI.setText(findTestObject('Register/Field_Whatsapp'), findTestData('Register').getValue(4, 1))

WebUI.setText(findTestObject('Register/Field_Kata Sandi'), findTestData('Register').getValue(5, 1))

WebUI.setText(findTestObject('Register/Field_Konfirmasi kata sandi'), findTestData('Register').getValue(5, 1))

WebUI.check(findTestObject('Register/Field_Check SK'))

WebUI.takeFullPageScreenshot()

WebUI.click(findTestObject('Register/Btn_Daftar'))

WebUI.takeFullPageScreenshot()

WebUI.closeBrowser()

