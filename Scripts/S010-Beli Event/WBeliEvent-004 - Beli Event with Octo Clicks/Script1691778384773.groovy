import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('WLog-001'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Home/Card_EVENT Day 4 Workshop bersama Ziyad Syauqi Fawwazi, Data Scientist Mark'))

WebUI.click(findTestObject('Etc/Btn_Beli Tiket Event Day 4'))

WebUI.click(findTestObject('Etc/Btn_Lihat Pembelian Saya'))

WebUI.takeFullPageScreenshot()

WebUI.click(findTestObject('Cart/Btn_Checkout'))

WebUI.click(findTestObject('Cart/Radio_Metode Pembayaran'))

WebUI.click(findTestObject('Cart/Btn_Confirm'))

WebUI.click(findTestObject('Cart/OCTO Clicks'))

WebUI.click(findTestObject('Cart/Btn_Back to merchant'))

WebUI.takeScreenshot()

WebUI.closeBrowser()

