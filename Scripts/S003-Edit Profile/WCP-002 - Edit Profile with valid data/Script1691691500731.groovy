import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('WCP-001'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Edit_Profile/Field_Fullname'), findTestData('Change Profile').getValue(1, 1))

WebUI.setText(findTestObject('Edit_Profile/Field_Phone'), findTestData('Change Profile').getValue(2, 1))

WebUI.setText(findTestObject('Edit_Profile/Field_BirthDay'), findTestData('Change Profile').getValue(3, 1))

WebUI.click(findTestObject('Edit_Profile/Field_Fullname'))

WebUI.takeScreenshot()

WebUI.click(findTestObject('Edit_Profile/Btn_Save Changes'))

WebUI.takeFullPageScreenshot()

WebUI.closeBrowser()

